﻿using ConsoleApp1.Models;
using Google.Cloud.BigQuery.V2;
using Microsoft.EntityFrameworkCore;
using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var projectId = "braided-tracker-288317";
            var resource = "bigquery-public-data";
            var datasource = "nppes";
            var tablename = "npi_optimized";

            var client = new BQClient(projectId);
            client.SetTable(resource, datasource, tablename);

            LoadAggregates(client);
            LoadDoctors(client, "NY", "NEW YORK", 1000);
        }

        public static void LoadAggregates(BQClient client)
        {
            var sql = @"
WITH
  doctors AS (
  SELECT
    npi,
    provider_first_name AS first_name,
    provider_last_name_legal_name AS last_name,
    provider_business_practice_location_address_city_name AS city,
    provider_business_practice_location_address_state_name AS state
  FROM
    {0}
  WHERE
    provider_last_name_legal_name != ''
    AND provider_business_practice_location_address_city_name != '' )
SELECT
  state,
  city,
  COUNT(*) AS `count`
FROM
  doctors
GROUP BY
  state,
  city";

            var results = client.GetData(sql);

            using (var context = new LocalContext())
            {
                context.ChangeTracker.AutoDetectChangesEnabled = false;
                context.Database.ExecuteSqlRaw("truncate table Aggregates");
                context.SaveChanges();

                var i = 0;
                foreach (BigQueryRow row in results)
                {
                    context.Aggregates.Add(new Aggregate
                    {
                        State = row["state"].ToString(),
                        City = row["city"].ToString(),
                        Count = int.Parse(row["count"].ToString()),
                    });
                    if (++i % 1000 == 0)
                        context.SaveChanges();
                }
                context.SaveChanges();
            }
        }

        public static void LoadDoctors(BQClient client, string state, string city, int? limit = null)
        {
            var sql = @"
WITH
  doctors AS (
  SELECT
    npi,
    provider_first_name AS first_name,
    provider_last_name_legal_name AS last_name,
    provider_business_practice_location_address_city_name AS city,
    provider_business_practice_location_address_state_name AS state,
    healthcare_provider_taxonomy_1_specialization AS specialization
  FROM
    {0}
  WHERE
    provider_last_name_legal_name != ''
    AND provider_business_practice_location_address_city_name != '' )
SELECT
  *
FROM
  doctors
WHERE
  state = @state
  AND city = @city";

            var parameters = new[]
            {
                new BigQueryParameter("state", BigQueryDbType.String, state),
                new BigQueryParameter("city", BigQueryDbType.String, city)
            };

            var results = client.GetData(sql, parameters, limit);

            using (var context = new LocalContext())
            {
                context.ChangeTracker.AutoDetectChangesEnabled = false;
                context.Database.ExecuteSqlRaw("truncate table Doctors");
                context.SaveChanges();

                foreach (BigQueryRow row in results)
                {
                    context.Doctors.Add(new Doctor
                    {
                        Npi = int.Parse(row["npi"].ToString()),
                        FirstName = row["first_name"].ToString(),
                        LastName = row["last_name"].ToString(),
                        Specializtion = row["specialization"].ToString()
                    });
                }
                context.SaveChanges();
            }
        }
    }
}
