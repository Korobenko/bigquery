﻿using Google.Cloud.BigQuery.V2;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class BQClient
    {
        private BigQueryClient client;
        public BQClient(string projectId)
        {
            client = BigQueryClient.Create(projectId);
        }

        public BigQueryResults GetData(string sql, BigQueryParameter[] parameters = null, int? limit = null)
        {
            var sql0 = string.Format(sql, table);
            if (limit.HasValue)
                sql0 += $" LIMIT {limit}";
            return client.ExecuteQuery(sql0, parameters);
        }

        private BigQueryTable table;
        public void SetTable(string resource, string datasource, string tablename)
        {
            table = client.GetTable(resource, datasource, tablename);
        }
    }
}
