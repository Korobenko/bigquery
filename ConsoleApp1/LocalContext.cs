﻿using ConsoleApp1.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public class LocalContext : DbContext
    {
        public DbSet<Doctor> Doctors { get; set; }
        public DbSet<Aggregate> Aggregates { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=localhost;Initial Catalog=BigQuery;Integrated Security=SSPI;MultipleActiveResultSets=true");
        }
    }
}
